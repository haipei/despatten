package com.kafeitoo.despatten.chain;

public class Women implements IWomen {
    /**
     * 女性的个人状况
     * 1 未出嫁
     * 2 出嫁
     * 3 夫死
     */
    private int type = 0;
    //女性的请示
    private String request = "";

    public Women(int type, String request) {
        this.type = type;
        switch (this.type) {
            case 1:
                this.request = "女儿的请求是：" + request;
            case 2:
                this.request = "妻子的请求是：" + request;
            case 3:
                this.request = "母亲的请求是：" + request;
        }
    }

    //获得女性的状况
    @Override
    public int getType() {
        return this.type;
    }

    //获得女性的请求
    @Override
    public String getRequest() {
        return this.request;
    }
}
