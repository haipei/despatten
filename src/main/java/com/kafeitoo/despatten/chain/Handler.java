package com.kafeitoo.despatten.chain;

public abstract class Handler {
    //父亲
    public final static int FATHER_LEVEL_REQUEST = 1;
    //丈夫
    public final static int HUSBAND_LEVEL_REQUEST = 2;
    //儿子
    public final static int SON_LEVEL_REQUEST = 3;
    //能处理的级别
    private int level = 0;
    //责任传递,下一个责任人是谁
    private Handler nextHandler;

    //每个类都要说明自己能处理哪些请求
    public Handler(int level) {
        this.level = level;
    }

    public final void HandlerMessage(IWomen women) {
        if (women.getType() == this.level) {
            this.response(women);
        } else {
            if (this.nextHandler != null) {
                //有后续环节，才传递请求
                this.nextHandler.HandlerMessage(women);
            } else {
                //没有人处理了
                System.out.println("没有人处理了，不同意！");
            }
        }
    }

    /**
     * 如果不属于自己的请求，则转移到下一个能处理的人
     *
     * @param _handler 处理人
     */
    public void setNext(Handler _handler) {
        this.nextHandler = _handler;
    }

    //有请示，当然有回应
    protected abstract void response(IWomen women);
}
