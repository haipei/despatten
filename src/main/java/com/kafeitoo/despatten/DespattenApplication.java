package com.kafeitoo.despatten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DespattenApplication {

	public static void main(String[] args) {
		SpringApplication.run(DespattenApplication.class, args);
	}
}
