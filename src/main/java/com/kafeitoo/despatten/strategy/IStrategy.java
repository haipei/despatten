package com.kafeitoo.despatten.strategy;

public interface IStrategy {
    void operate();
}
