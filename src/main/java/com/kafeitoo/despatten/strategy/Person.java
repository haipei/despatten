package com.kafeitoo.despatten.strategy;

public class Person {
    public static void main(String[] args) {
        Context context;
        System.out.println("首先要认对路");
        context = new Context(new BackDoor());
        context.operate();
        System.out.println("然后送好处");
        context = new Context(new GivenGreenLight());
        context.operate();
        System.out.println("最后要跑路");
        context = new Context(new BlockEnemy());
        context.operate();
    }
}
