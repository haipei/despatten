package com.kafeitoo.despatten.strategy;

public class Context {
    private IStrategy iStrategy;

    public Context(IStrategy iStrategy) {
        this.iStrategy = iStrategy;
    }

    void operate(){
        this.iStrategy.operate();
    }
}
