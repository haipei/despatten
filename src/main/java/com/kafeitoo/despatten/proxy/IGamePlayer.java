package com.kafeitoo.despatten.proxy;

public interface IGamePlayer {
    void login(String user, String password);

    void killBoss();

    void upgrade();
}
