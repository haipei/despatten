package com.kafeitoo.despatten.proxy;

import java.time.LocalDateTime;
import java.util.Locale;

public class Client {
    public static void main(String[] args) {
        IGamePlayer player = new GamePlayer("裴叔");

        IGamePlayer proxy = new GamePlayerProxy(player);

        System.out.println(LocalDateTime.now());
        proxy.login("xiaoyu", "123456");
        proxy.killBoss();
        proxy.upgrade();
        System.out.println(LocalDateTime.now());
    }
}
