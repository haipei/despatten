package com.kafeitoo.despatten.iterator;

public class Boss {
    public static void main(String[] args) {
        IProject project = new Project();
        project.add("星球大战", 10, 1999);
        project.add("张二狗子", 100, 2999);
        project.add("孙二狗子", 1000, 3999);
        for (int i = 4; i < 104; i++) {
            project.add("第" + i + "个狗子", i * 5, i * 12390);
        }

        IProjectIterator iterator = project.iterator();
        while (iterator.hasNext()) {
            IProject next = (IProject) iterator.next();
            System.out.println(next.getProjectInfo());
        }
    }
}
