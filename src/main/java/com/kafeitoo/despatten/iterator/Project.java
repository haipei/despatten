package com.kafeitoo.despatten.iterator;

import java.util.ArrayList;

public class Project implements IProject {
    private ArrayList<IProject> projects = new ArrayList<>();

    private String name = "";
    private int num = 0;
    private int cost = 0;

    public Project() {
    }

    public Project(String name, int num, int cost) {
        this.name = name;
        this.num = num;
        this.cost = cost;
    }

    @Override
    public String getProjectInfo() {
        String info = "项目名称是：" + this.name + "\t项目人数:" + this.num + "\t项目费用:" + this.cost;
        return info;
    }

    @Override
    public void add(String name, int num, int cost) {
        this.projects.add(new Project(name, num, cost));
    }

    @Override
    public IProjectIterator iterator() {
        return new ProjectIterator(this.projects);
    }
}
