package com.kafeitoo.despatten.composite;

import java.util.ArrayList;

public class Client {
    public static void main(String[] args) {
        IRoot ceo = new Root("王大麻子", "总经理", 100000);

        IBranch developDep = new Branch("刘大帅", "研发经理", 10000);
        IBranch salesDep = new Branch("张大大", "销售头子", 20000);
        IBranch financeDep = new Branch("孙胖胖", "财务大佬", 30000);

        IBranch firstDevGroup = new Branch("wz", "一组妹子", 8000);
        IBranch secondDevGroup = new Branch("yx", "二组汉子", 8000);

        ILeaf a = new Leaf("a", "开发人员", 2000);
        ILeaf b = new Leaf("b", "开发人员", 2000);
        ILeaf c = new Leaf("c", "开发人员", 2000);
        ILeaf d = new Leaf("d", "开发人员", 2000);
        ILeaf e = new Leaf("e", "开发人员", 2000);
        ILeaf f = new Leaf("f", "销售人员", 4000);
        ILeaf g = new Leaf("g", "销售人员", 2000);
        ILeaf h = new Leaf("h", "财务人员", 5000);
        ILeaf k = new Leaf("k", "财务人员", 5000);
        ILeaf l = new Leaf("l", "CEO秘书", 8000);

        ILeaf ph = new Leaf("ph", "研发部二把手", 10000);

        ceo.add(developDep);
        ceo.add(salesDep);
        ceo.add(financeDep);

        ceo.add(l);

        developDep.add(firstDevGroup);
        developDep.add(secondDevGroup);

        developDep.add(ph);

        firstDevGroup.add(a);
        firstDevGroup.add(b);
        firstDevGroup.add(c);

        secondDevGroup.add(d);
        secondDevGroup.add(e);

        salesDep.add(f);
        salesDep.add(g);

        financeDep.add(h);
        financeDep.add(k);

        System.out.println(ceo.getInfo());

        getAllSubordinateInfo(ceo.getSubordinateInfo());

    }

    private static void getAllSubordinateInfo(ArrayList subordinateList) {
        int length = subordinateList.size();
        for (int i = 0; i < length; i++) {
            Object o = subordinateList.get(i);
            if (o instanceof Leaf) {
                ILeaf employee = (ILeaf) o;
                System.out.println(employee.getInfo());
            } else {
                IBranch branch = (IBranch) o;
                System.out.println(branch.getInfo());
                getAllSubordinateInfo(branch.getSubordinateInfo());
            }
        }
    }
}
