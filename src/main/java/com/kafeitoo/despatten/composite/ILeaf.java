package com.kafeitoo.despatten.composite;

public interface ILeaf {
    public String getInfo();
}
