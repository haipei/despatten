package com.kafeitoo.despatten.factory.simple;

import com.kafeitoo.despatten.factory.BlackHuman;
import com.kafeitoo.despatten.factory.IHuman;
import com.kafeitoo.despatten.factory.WhiteHuman;
import com.kafeitoo.despatten.factory.YellowHuman;

public class SimpleNvWa {
    public static void main(String[] args) {
        IHuman whiteHuman = SimpleHumanFactory.createHuman(WhiteHuman.class);
        IHuman blackHuman = SimpleHumanFactory.createHuman(BlackHuman.class);
        IHuman yellowHuman = SimpleHumanFactory.createHuman(YellowHuman.class);

        whiteHuman.getColor();
        whiteHuman.talk();

        blackHuman.getColor();
        blackHuman.talk();

        yellowHuman.getColor();
        yellowHuman.talk();
    }
}
