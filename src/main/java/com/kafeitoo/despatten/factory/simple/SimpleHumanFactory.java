package com.kafeitoo.despatten.factory.simple;

import com.kafeitoo.despatten.factory.IHuman;

/**
 * 简单工厂,也叫做静态工厂
 */
public class SimpleHumanFactory {
    public static <T extends IHuman> T createHuman(Class<T> c) {
        IHuman human = null;
        try {
            human = (IHuman) Class.forName(c.getName()).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return (T) human;
    }
}
