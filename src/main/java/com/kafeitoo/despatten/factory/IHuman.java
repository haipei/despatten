package com.kafeitoo.despatten.factory;

public interface IHuman {
    void getColor();

    void talk();
}
