package com.kafeitoo.despatten.factory.multi;

import com.kafeitoo.despatten.factory.AbstractHumanFactory;
import com.kafeitoo.despatten.factory.IHuman;
import com.kafeitoo.despatten.factory.YellowHuman;

public class YellowHumanFactory extends AbstractHumanFactory {
    @Override
    public <T extends IHuman> T createHuman(Class<T> Clazz) {
        return null;
    }

    @Override
    public IHuman createHuman() {
        return new YellowHuman();
    }
}
