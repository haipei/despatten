package com.kafeitoo.despatten.factory.multi;

import com.kafeitoo.despatten.factory.IHuman;

public class MultiNvWa {
    public static void main(String[] args) {
        IHuman whiteHuman = new WhtieHumanFactory().createHuman();
        IHuman blackHuman = new BlackHumanFactory().createHuman();
        IHuman yellowHuman = new YellowHumanFactory().createHuman();

        whiteHuman.getColor();
        blackHuman.getColor();
        yellowHuman.getColor();

    }
}
