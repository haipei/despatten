package com.kafeitoo.despatten.factory;

public class BlackHuman implements IHuman {
    @Override
    public void getColor() {
        System.out.println("黑娃黑皮肤");
    }

    @Override
    public void talk() {
        System.out.println("黑人说：叽里咕噜，哇啦哇啦，五色丝！");
    }
}
