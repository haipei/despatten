package com.kafeitoo.despatten.factory.lazy;

import java.util.HashMap;
import java.util.Map;

/**
 * 延迟加载工厂类
 */
public class ProductFactory {
    private static final Map<String, Product> PRODUCT_MAP = new HashMap<>();

    public static synchronized Product createProduct(String type) {
        Product product = null;
        //如果map中已经有这个对象
        if (PRODUCT_MAP.containsKey(type)) {
            product = PRODUCT_MAP.get(type);
            System.out.println("已经存在["+type+"]，直接返回。");
        } else {
            if (type.equals("Product1")) {
                product = new ConcreteProduct1();
            } else {
                product = new ConcreteProduct2();
            }
            PRODUCT_MAP.put(type, product);
        }
        return product;
    }

    public static void main(String[] args) {
        createProduct("Product1");
        createProduct("Product1");
        createProduct("Product2");
    }
}
