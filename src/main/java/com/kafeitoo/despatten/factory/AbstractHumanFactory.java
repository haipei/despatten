package com.kafeitoo.despatten.factory;

public abstract class AbstractHumanFactory {
    public abstract <T extends IHuman> T createHuman(Class<T> Clazz);

    public abstract IHuman createHuman();
}
