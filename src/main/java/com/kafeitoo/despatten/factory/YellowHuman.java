package com.kafeitoo.despatten.factory;

public class YellowHuman implements IHuman {
    @Override
    public void getColor() {
        System.out.println("黑眼睛黑头发黄皮肤...");
    }

    @Override
    public void talk() {
        System.out.println("永永远远龙的传人...");
    }
}
