package com.kafeitoo.despatten.factory;

public class HumanFactory extends AbstractHumanFactory {
    @Override
    public <T extends IHuman> T createHuman(Class<T> Clazz) {
        IHuman human = null;
        try {
            human = (T)Class.forName(Clazz.getName()).newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return (T) human;
    }

    @Override
    public IHuman createHuman() {
        return null;
    }
}
