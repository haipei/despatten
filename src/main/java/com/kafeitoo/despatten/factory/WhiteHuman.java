package com.kafeitoo.despatten.factory;

public class WhiteHuman implements IHuman {
    @Override
    public void getColor() {
        System.out.println("白大夫，就是让你白");
    }

    @Override
    public void talk() {
        System.out.println("白色人说：hello");
    }
}
